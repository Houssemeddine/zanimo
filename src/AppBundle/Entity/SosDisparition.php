<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SosDisparition
 *
 * @ORM\Table(name="sos_disparition")
 * @ORM\Entity
 */
class SosDisparition
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_sos_disparition", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idSosDisparition;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_membre", type="integer", nullable=false)
     */
    private $idMembre;


}

