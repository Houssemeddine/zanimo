<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Nouriture
 *
 * @ORM\Table(name="nouriture")
 * @ORM\Entity
 */
class Nouriture
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_produits", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idProduits;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_nouriture", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idNouriture;


}

