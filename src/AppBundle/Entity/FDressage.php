<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FDressage
 *
 * @ORM\Table(name="f_dressage", indexes={@ORM\Index(name="id_membre", columns={"id_membre"})})
 * @ORM\Entity
 */
class FDressage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_f_Dressage", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idFDressage;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_membre", type="integer", nullable=false)
     */
    private $idMembre;

    /**
     * @var string
     *
     * @ORM\Column(name="espece", type="string", length=254, nullable=true)
     */
    private $espece;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=254, nullable=true)
     */
    private $nom;

    /**
     * @var float
     *
     * @ORM\Column(name="poids", type="float", precision=10, scale=0, nullable=true)
     */
    private $poids;

    /**
     * @var string
     *
     * @ORM\Column(name="proprietaire", type="string", length=250, nullable=true)
     */
    private $proprietaire;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=250, nullable=true)
     */
    private $photo;

    /**
     * @var integer
     *
     * @ORM\Column(name="displine", type="integer", nullable=true)
     */
    private $displine;

    /**
     * @var integer
     *
     * @ORM\Column(name="obeissance", type="integer", nullable=true)
     */
    private $obeissance;

    /**
     * @var string
     *
     * @ORM\Column(name="specialite", type="string", length=250, nullable=true)
     */
    private $specialite;

    /**
     * @var integer
     *
     * @ORM\Column(name="accompagnement", type="integer", nullable=true)
     */
    private $accompagnement;

    /**
     * @var integer
     *
     * @ORM\Column(name="interception", type="integer", nullable=true)
     */
    private $interception;

    /**
     * @var float
     *
     * @ORM\Column(name="noteTotal", type="float", precision=10, scale=0, nullable=true)
     */
    private $notetotal;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDebut", type="date", nullable=true)
     */
    private $datedebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datFin", type="date", nullable=true)
     */
    private $datfin;


}

