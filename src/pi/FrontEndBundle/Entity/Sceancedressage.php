<?php

namespace pi\FrontEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sceancedressage
 *
 * @ORM\Table(name="sceancedressage", indexes={@ORM\Index(name="AK_id_animal", columns={"id_membre", "id_animal"})})
 * @ORM\Entity
 */
class Sceancedressage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_membre", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idMembre;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_animal", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idAnimal;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $date;


}

