<?php

namespace pi\FrontEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FSoin
 *
 * @ORM\Table(name="f_soin", indexes={@ORM\Index(name="id_membre", columns={"id_membre"})})
 * @ORM\Entity
 */
class FSoin
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_f_Soin", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idFSoin;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_membre", type="integer", nullable=false)
     */
    private $idMembre;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=250, nullable=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="espece", type="string", length=250, nullable=true)
     */
    private $espece;

    /**
     * @var string
     *
     * @ORM\Column(name="poids", type="string", length=250, nullable=true)
     */
    private $poids;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datePoids", type="date", nullable=true)
     */
    private $datepoids;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="neLe", type="date", nullable=true)
     */
    private $nele;

    /**
     * @var string
     *
     * @ORM\Column(name="genre", type="string", length=250, nullable=true)
     */
    private $genre;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="string", length=250, nullable=true)
     */
    private $observation;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=250, nullable=true)
     */
    private $photo;

    /**
     * @var string
     *
     * @ORM\Column(name="medicament", type="string", length=250, nullable=true)
     */
    private $medicament;

    /**
     * @var string
     *
     * @ORM\Column(name="proprietaire", type="string", length=250, nullable=true)
     */
    private $proprietaire;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="prochainRDV", type="date", nullable=true)
     */
    private $prochainrdv;


}

