<?php

namespace pi\FrontEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Adoption
 *
 * @ORM\Table(name="adoption", indexes={@ORM\Index(name="FK_membreAdoption", columns={"id_membre"})})
 * @ORM\Entity
 */
class Adoption
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_adoption", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idAdoption;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_membre", type="integer", nullable=true)
     */
    private $idMembre;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=11, nullable=true)
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAnnonce", type="date", nullable=true)
     */
    private $dateannonce;

    /**
     * @var string
     *
     * @ORM\Column(name="lieu", type="string", length=50, nullable=true)
     */
    private $lieu;

    /**
     * @var integer
     *
     * @ORM\Column(name="etatAdoption", type="integer", nullable=true)
     */
    private $etatadoption;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_animal", type="integer", nullable=true)
     */
    private $idAnimal;


}

